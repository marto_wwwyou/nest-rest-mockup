import { Module } from '@nestjs/common';
import { PasswordHashService } from './services/password.hash.service';

@Module({
  providers: [PasswordHashService],
  exports: [PasswordHashService],
})
export class SharedModule {}
