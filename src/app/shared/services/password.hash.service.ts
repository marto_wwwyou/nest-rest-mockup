import * as bcrypt from 'bcrypt';
import { Injectable } from '@nestjs/common';

@Injectable()
export class PasswordHashService {
  private saltRounds = 10;

  public make(text: string): Promise<string> {
    return bcrypt.hash(text, this.saltRounds);
  }

  public compare(hash: string, text: string): Promise<boolean> {
    return bcrypt.compare(text, hash);
  }
}
