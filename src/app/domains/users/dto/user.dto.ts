import { IsEmail, IsNotEmpty, MinLength, Validate } from 'class-validator';
import { UserEmailExsists } from '../validators/user.exists.validator';

export class UserDto {
  @IsNotEmpty()
  @IsEmail()
  @Validate(UserEmailExsists, {
    message: 'User with such email already exists',
  })
  readonly email: string;

  @IsNotEmpty()
  @MinLength(5)
  readonly password: string;
}
