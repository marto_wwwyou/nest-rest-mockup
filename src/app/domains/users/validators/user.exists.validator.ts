import {
  ValidationArguments,
  ValidatorConstraintInterface,
  ValidatorConstraint,
} from 'class-validator';
import { UsersRepository } from '../repositories/users.repository';
import { Injectable } from '@nestjs/common';

@ValidatorConstraint({ name: 'UserEmailExsists', async: true })
@Injectable()
export class UserEmailExsists implements ValidatorConstraintInterface {
  constructor(private readonly usersRepository: UsersRepository) {}

  async validate(email: string, args: ValidationArguments) {
    const user = await this.usersRepository.findOneByEmail(email);
    return !user;
  }

  defaultMessage(args: ValidationArguments) {
    return 'User with such email already exissts.';
  }
}
