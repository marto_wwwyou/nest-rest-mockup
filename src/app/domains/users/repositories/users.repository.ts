import { Repository, EntityRepository } from 'typeorm';
import { UserEntity } from '../entities/user.entity';
import { Injectable } from '@nestjs/common';

@Injectable()
@EntityRepository(UserEntity)
export class UsersRepository extends Repository<UserEntity> {
  public findOneByEmail(email: string): Promise<UserEntity | undefined> {
    return this.findOne({ email });
  }
}
