import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ObjectLiteral,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity({
  name: 'users',
})
export class UserEntity implements ObjectLiteral {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column({ length: 160 })
  public email: string;

  @Column({ length: 160 })
  public password: string;

  @CreateDateColumn({ name: 'created_on', type: 'timestamp' })
  public createdOn: Date;

  @UpdateDateColumn({ name: 'updated_on', type: 'timestamp' })
  public updatedOn: Date;
}
