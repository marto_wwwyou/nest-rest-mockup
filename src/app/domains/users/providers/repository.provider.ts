import { Connection } from 'typeorm';
import { UsersRepository } from '../repositories/users.repository';

export const UsersRepositoryProvider = {
  provide: 'UsersRepository',
  useFactory: (connection: Connection) =>
    connection.getCustomRepository(UsersRepository),
  inject: [Connection],
};
