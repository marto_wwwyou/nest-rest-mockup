import { Module } from '@nestjs/common';
import { UsersRepository } from './repositories/users.repository';
import { UserDto } from './dto/user.dto';
import { UserEntity } from './entities/user.entity';
import { UsersRepositoryProvider } from './providers/repository.provider';
import { UserEmailExsists } from './validators/user.exists.validator';
import { SharedModule } from 'src/app/shared/shared.module';

@Module({
  imports: [SharedModule],
  providers: [UsersRepositoryProvider, UserDto, UserEntity, UserEmailExsists],
  exports: [UserDto, UserEntity, UsersRepositoryProvider],
})
export class UsersModule {}
