import { Module } from '@nestjs/common';
import { UiModule } from './ui/ui.module';
import { UsersModule } from './domains/users/users.module';
import { SharedModule } from './shared/shared.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { config } from '../config/app';
@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      useFactory: () => config.database,
    }),
    SharedModule,
    UiModule,
    UsersModule,
  ],
})
export class AppModule {}
