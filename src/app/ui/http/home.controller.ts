import { Controller, Get } from '@nestjs/common';
import { config } from '../../../config/app';

@Controller()
export class HomeController {
  @Get()
  public index() {
    return {
      version: config.version,
    };
  }
}
