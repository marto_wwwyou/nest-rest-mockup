import { Controller, Get, Post, Patch, Body, Param, Res } from '@nestjs/common';
import { UserDto } from 'src/app/domains/users/dto/user.dto';
import { Response } from 'express';
import { UsersRepository } from 'src/app/domains/users/repositories/users.repository';
import { EntityManager } from 'typeorm';
import { PasswordHashService } from 'src/app/shared/services/password.hash.service';
import { UserEntity } from 'src/app/domains/users/entities/user.entity';

@Controller('users')
export class UsersController {
  constructor(
    private readonly usersRepository: UsersRepository,
    private readonly hashService: PasswordHashService,
  ) {}

  @Get()
  public index() {
    return this.usersRepository.find();
  }

  @Post()
  public async create(@Body() user: UserDto) {
    const userEntity: UserEntity = new UserEntity();
    userEntity.email = user.email;
    userEntity.password = await this.hashService.make(user.password);
    const savedUser = await this.usersRepository.save(userEntity);

    return {
      success: true,
      id: savedUser.id,
    };
  }

  @Patch('/:id')
  public update(
    @Body() user: UserDto,
    @Param('id') id: number,
    @Res() response: Response,
  ) {}

  @Patch('/:id')
  public delete(@Param('id') id: number, @Res() response: Response) {}
}
