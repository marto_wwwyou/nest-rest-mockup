import { Module } from '@nestjs/common';
import { HomeController } from './http/home.controller';
import { UsersController } from './http/users.controller';
import { UsersModule } from '../domains/users/users.module';
import { SharedModule } from '../shared/shared.module';

@Module({
  imports: [UsersModule, SharedModule],
  controllers: [HomeController, UsersController],
})
export class UiModule {}
