import * as cors from 'cors';
import * as helmet from 'helmet';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import {
  INestApplication,
  INestMicroservice,
  ValidationPipe,
} from '@nestjs/common';
import { AppModule } from './app.module';
import { useContainer } from 'class-validator';
import { config } from '../config/app';

export class AppDispatcher {
  private app: INestApplication;
  private microservice: INestMicroservice;

  async dispatch(): Promise<void> {
    await this.createServer();
    return this.startServer();
  }

  async shutdown(): Promise<void> {
    await this.app.close();
  }

  private async createServer(): Promise<void> {
    this.app = await NestFactory.create(AppModule);
    useContainer(this.app.select(AppModule), { fallbackOnErrors: true });
    this.app.useGlobalPipes(
      new ValidationPipe({
        transform: true,
      }),
    );
    this.app.use(cors());
    if (config.isProduction) {
      // get from config
      this.app.use(helmet());
    }
    const options = new DocumentBuilder()
      .setTitle(config.name) // get from config
      .setDescription(config.description) // get from config
      .setVersion(config.version) // get from config
      .addBearerAuth()
      .build();

    const document = SwaggerModule.createDocument(this.app, options);
    SwaggerModule.setup('/swagger', this.app, document);
  }

  private async startServer(): Promise<void> {
    await this.app.listen(config.port, config.host);
  }
}
