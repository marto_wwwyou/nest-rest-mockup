require('dotenv-safe').load();

import { AppDispatcher } from './app/app.disptacher';

const dispatcher = new AppDispatcher();
dispatcher.dispatch();

process.on('SIGINT', async () => {
  await dispatcher.shutdown();
  process.exit();
});
