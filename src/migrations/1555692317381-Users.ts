import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class Users1555692317381 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.createTable(
      new Table({
        name: 'users',
        columns: [
          {
            name: 'id',
            type: 'int',
            isPrimary: true,
            length: '10',
            unsigned: true,
            isGenerated: true,
            generationStrategy: 'increment',
          },
          {
            name: 'email',
            type: 'varchar',
            length: '160',
            isNullable: false,
          },
          {
            name: 'password',
            type: 'varchar',
            length: '160',
            isNullable: false,
          },
          {
            name: 'created_on',
            type: 'int',
            length: '11',
            isNullable: false,
            unsigned: true,
          },
          {
            name: 'updated_on',
            type: 'int',
            length: '11',
            isNullable: true,
            unsigned: true,
          },
        ],
      }),
      true,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.dropTable('users');
  }
}
