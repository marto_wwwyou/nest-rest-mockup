import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class Catalogue1555769871205 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.createTable(
      new Table({
        name: 'products',
        columns: [
          {
            name: 'id',
            type: 'int',
            isPrimary: true,
            length: '10',
            unsigned: true,
            isGenerated: true,
            generationStrategy: 'increment',
          },
          {
            name: 'name',
            type: 'varchar',
            length: '200',
            isNullable: false,
          },
          {
            name: 'description',
            type: 'text',
            isNullable: true,
          },
          {
            name: 'picture_id',
            type: 'varchar',
            length: '200',
            isNullable: false,
          },
          {
            name: 'status',
            type: 'enum',
            enum: ['published', 'draft', 'in_review'],
            default: '"draft"',
          },
          {
            name: 'price',
            type: 'bigint',
            unsigned: true,
            length: '20',
            isNullable: false,
            default: '0',
          },
          {
            name: 'created_on',
            type: 'int',
            length: '11',
            isNullable: false,
            unsigned: true,
          },
          {
            name: 'updated_on',
            type: 'int',
            length: '11',
            isNullable: true,
            unsigned: true,
          },
        ],
      }),
    );

    await queryRunner.createTable(
      new Table({
        name: 'categories',
        columns: [
          {
            name: 'id',
            type: 'int',
            isPrimary: true,
            length: '10',
            unsigned: true,
            isGenerated: true,
            generationStrategy: 'increment',
          },
          {
            name: 'name',
            type: 'varchar',
            length: '200',
            isNullable: false,
          },
          {
            name: 'parent_id',
            type: 'int',
            length: '10',
            unsigned: true,
            isNullable: false,
          },
          {
            name: 'left_node',
            type: 'int',
            length: '10',
            unsigned: true,
            isNullable: false,
          },
          {
            name: 'right_node',
            type: 'int',
            length: '10',
            unsigned: true,
            isNullable: false,
          },
          {
            name: 'created_on',
            type: 'int',
            length: '11',
            isNullable: false,
            unsigned: true,
          },
          {
            name: 'updated_on',
            type: 'int',
            length: '11',
            isNullable: true,
            unsigned: true,
          },
        ],
      }),
    );

    await queryRunner.createTable(
      new Table({
        name: 'product_categories',
        columns: [
          {
            name: 'product_id',
            type: 'int',
            isPrimary: true,
            length: '10',
            unsigned: true,
          },
          {
            name: 'category_id',
            type: 'int',
            isPrimary: true,
            length: '10',
            unsigned: true,
          },
        ],
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.dropTable('products');
    await queryRunner.dropTable('categories');
    await queryRunner.dropTable('product_categories');
  }
}
